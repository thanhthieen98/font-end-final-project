import {
  ChangePassword,
  ForgotPassword,
  Home,
  ListProducts,
  PageNotFound,
  Profile,
  SignIn,
  SignUp,
} from "page";
import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { getLocalStorage } from "utils/localStorage";
import "./App.scss";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/list-product" component={ListProducts} />
        <PrivateRoute exact path="/profile" component={Profile} />
        <PrivateRoute
          exact
          path="/change-password"
          component={ChangePassword}
        />
        <AuthRoute exact path="/sign-in" component={SignIn} />
        <AuthRoute exact path="/sign-up" component={SignUp} />
        <AuthRoute exact path="/forgot-password" component={ForgotPassword} />
        <Route path="*" component={PageNotFound} />
      </Switch>
    </div>
  );
}

export default App;

function PrivateRoute({
  component: TargetPage,
  ...rest
}: any): React.ReactElement {
  return (
    <Route
      {...rest}
      render={(props) =>
        getLocalStorage("token", 0) ? (
          <TargetPage {...props} />
        ) : (
          <Redirect to={{ pathname: "/sign-in" }} />
        )
      }
    />
  );
}
function AuthRoute({
  component: TargetPage,
  ...rest
}: any): React.ReactElement {
  return (
    <Route
      {...rest}
      render={(props) =>
        !getLocalStorage("token", 0) ? (
          <TargetPage {...props} />
        ) : (
          <Redirect to={{ pathname: "/" }} />
        )
      }
    />
  );
}
