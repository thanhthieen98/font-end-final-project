import { mainAxios } from "config/axios";

export const signIn = async (payload: { email: string; password: string }) => {
  try {
    const res: any = await mainAxios.postRequest({
      url: "/auth/login",
      payload,
    });
    return res;
  } catch (error) {
    return null;
  }
};

export const signUp = async (payload: any) => {
  try {
    const res: any = await mainAxios.postRequest({
      url: "/auth/register",
      payload,
    });
    return res;
  } catch (error) {
    return null;
  }
};

export const changePassword = async (payload: any) => {
  try {
    const res: any = await mainAxios.postRequest({
      url: "/auth/change-password",
      payload,
      requiresToken: true,
    });
    return res;
  } catch (error) {
    return null;
  }
};

export const setupPasswordGoogle = async (payload: any) => {
  try {
    const res: any = await mainAxios.postRequest({
      url: "/auth/setup-password-google",
      payload: { password: payload.confirm },
      requiresToken: true,
    });
    return res;
  } catch (error) {
    return null;
  }
};

export const loginGoogle = async (payload: any) => {
  try {
    const res: any = await mainAxios.postRequest({
      url: "/auth/login-google",
      payload: {
        user: payload,
      },
    });
    return res;
  } catch (error) {
    return null;
  }
};
