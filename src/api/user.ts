import { mainAxios } from "config/axios";

export const getProfile = async () => {
  try {
    const res: any = await mainAxios.getRequest({
      url: "/user/profile",
      requiresToken: true,
    });
    return res;
  } catch (error) {
    return null;
  }
};
export const updateProfile = async (payload: any) => {
  try {
    const res: any = await mainAxios.postRequest({
      url: "/user/update-user",
      requiresToken: true,
      payload,
    });
    return res;
  } catch (error) {
    return null;
  }
};
