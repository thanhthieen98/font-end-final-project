import { Button, Card } from "antd";
import React from "react";

const { Meta } = Card;

const CardItem = () => {
  return (
    <Card
      cover={
        <img
          alt="example"
          src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"
        />
      }
      actions={[
        <Button key="buy">Mua</Button>,
        <Button key="eye">Chi tiết</Button>,
      ]}
    >
      <Meta title="Europe Street beat" description="www.instagram.com" />
    </Card>
  );
};

export default CardItem;
