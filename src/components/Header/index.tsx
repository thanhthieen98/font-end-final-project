import {
  CaretDownOutlined,
  LogoutOutlined,
  PlusSquareOutlined,
  ProfileOutlined,
  ShoppingCartOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Button, Col, Dropdown, Input, Menu, Row } from "antd";
import React, { memo } from "react";
import { Link } from "react-router-dom";
import logo from "asset/img/logo.png";
import { cleanLocalStorage, getLocalStorage } from "utils/localStorage";
import "./style.scss";
import { useGoogleLogout } from "react-google-login";

interface HeaderProps {}

const itemAuth = [
  {
    key: "/profile",
    name: "Thông tin tài khoản",
    icon: <ProfileOutlined />,
  },
  {
    key: "/",
    name: "Đăng xuất",
    icon: <LogoutOutlined />,
    action: true,
  },
];

const itemNotAuth = [
  {
    key: "/sign-in",
    name: "Đăng nhập",
    icon: <UserOutlined />,
  },
  {
    key: "/sign-up",
    name: "Đăng ký",
    icon: <PlusSquareOutlined />,
  },
];

const HeaderChildren: React.FC<HeaderProps> = () => {
  const user = getLocalStorage("user", 0);
  const { signOut } = useGoogleLogout({
    clientId: process.env.REACT_APP_GOOGLE_CLIENT_ID!,
  });

  const menu = (
    <Menu>
      {user
        ? itemAuth.map((item) => (
            <Menu.Item
              className="menu-item"
              key={item.key}
              onClick={() => item.action && cleanLocalStorage() && signOut()}
            >
              <Link to={item.key}>
                {item.icon}
                {item.name}
              </Link>
            </Menu.Item>
          ))
        : itemNotAuth.map((item) => (
            <Menu.Item key={item.key} className="menu-item">
              <Link to={item.key}>
                {item.icon}
                {item.name}
              </Link>
            </Menu.Item>
          ))}
    </Menu>
  );

  return (
    <>
      <Row gutter={[16, 16]} align="middle">
        <Col span={6}>
          <img className="logo" src={logo} alt="" />
        </Col>
        <Col span={13}>
          <Input.Search
            className="input-search"
            size="middle"
            placeholder="Nhập từ khoá tìm kiếm"
          />
        </Col>
        <Col span={5}>
          <Row justify="space-between">
            <Col>
              <Dropdown overlay={menu} trigger={["click"]}>
                <Button
                  className="dropdown"
                  type="text"
                  style={{ marginLeft: 40 }}
                >
                  {user ? user.name : "Tài khoản"}
                  <CaretDownOutlined />
                </Button>
              </Dropdown>
            </Col>
            <Col>
              <ShoppingCartOutlined
                style={{ fontSize: 25, color: "#a8cf45" }}
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
};

export default memo(HeaderChildren);
