import { DoubleRightOutlined, UnorderedListOutlined } from "@ant-design/icons";
import { Menu } from "antd";
import React, { memo } from "react";
import { useHistory } from "react-router-dom";

export interface MenuSideBarProps {
  data: {
    list: { key: string; name: string }[];
    title: string;
  };
}

const MenuSideBar: React.FC<MenuSideBarProps> = memo(({ data }) => {
  const history: any = useHistory();

  return (
    <>
      <p className="category">
        <UnorderedListOutlined /> {data.title}
      </p>
      <Menu
        onClick={({ key }) => history?.push(key)}
        defaultActiveFirst
        activeKey={history?.location?.pathname}
      >
        {data.list.map((item) => (
          <Menu.Item
            key={item.key}
            icon={<DoubleRightOutlined style={{ fontSize: 12 }} />}
          >
            {item.name}
          </Menu.Item>
        ))}
      </Menu>
    </>
  );
});

export default MenuSideBar;
