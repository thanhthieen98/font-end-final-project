import React from "react";
import { Link } from "react-router-dom";
import "./style.scss";

const Menu = () => {
  return (
    <div className="list-menu">
      <div className="container">
        <ul>
          <li>
            <Link to="/">trang chủ</Link>
          </li>
          <li>
            <Link to="/list-product">sản phẩm</Link>
          </li>
          <li>
            <Link to="/">tin tức</Link>
          </li>
          <li>
            <Link to="/">giới thiệu</Link>
          </li>
          <li>
            <Link to="/">liên hệ</Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default React.memo(Menu);
