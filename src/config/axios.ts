import { message } from "antd";
import axios from "axios";
import { cleanLocalStorage, getLocalStorage } from "utils/localStorage";
import { createBrowserHistory } from "history";
import { parameterType } from "./type";

const history = createBrowserHistory();

const _axios = axios.create({
  baseURL: "http://localhost:8000",
});

_axios.interceptors.request.use(
  (config: any) => {
    if (config.data) {
      const haveFile = Object.values(config.data).some(
        (e: any) => e && e.toString() === "[object File]"
      );
      if (haveFile) {
        config.headers["Content-Type"] = "multipart/form-data";
      }
    }

    return config;
  },
  (error) => Promise.reject(error)
);

_axios.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response && error.response.status) {
      if (error.response.status === 403) {
        cleanLocalStorage();
        // window.location.href = "/sign-in";
        history.push("/sign-in");
        message.warning("Phiên đăng nhập hết hạn");
        window.location.reload();
      }
      // if (error?.response?.data?.error === "invalid_token") {
      //   cleanLocaStorage();
      //   // history.push("/login");
      //   window.location.reload();
      // }
      return Promise.reject({ ...error.response.data });
    } else {
      return Promise.reject({
        error: true,
        message: "Error Code 100: No response error from server",
        statusCode:
          error && error.request && error.request.status
            ? error.request.status
            : "1899",
      });
    }
  }
);

const mainAxios = {
  request: async ({
    methodType,
    url,
    payload,
    requiresToken = false,
    config,
  }: parameterType) => {
    return new Promise((resolve, reject) => {
      // axios request default options
      const headers = config && config.headers ? config.headers : {};

      if (headers.contentType) {
        headers["Content-Type"] = headers.contentType;
        delete headers.contentType;
      } else {
        headers["Content-Type"] = "application/json";
      }

      // if API endpoint requires a token
      if (requiresToken) {
        const acToken = getLocalStorage("token", 0);
        if (acToken) headers["Authorization"] = `Bearer ${acToken}`;
      }

      _axios
        .request({
          url,
          method: methodType,
          data: payload,
          headers,
        })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          if (err) {
            if (err.statusCode === 401) {
              const acToken = getLocalStorage("token", 0);
              if (acToken) {
                localStorage.removeItem("token");
                window.location.reload();
              }
            }
            if (err.statusCode >= 400 && err.statusCode < 500) {
              // alert(errData.message)
              if (typeof err.message === "string") message.error(err.message);
            } else {
              message.error("Something wrong!");
              // alert('서버에러:' + errData.message)
            }
          }
          reject(err);
        });
    });
  },

  getRequest: async function (parameter: parameterType) {
    parameter.methodType = "GET";
    return this.request(parameter);
  },

  postRequest: async function (parameter: parameterType) {
    parameter.methodType = "POST";
    return this.request(parameter);
  },

  putRequest: async function (parameter: parameterType) {
    parameter.methodType = "PUT";
    return this.request(parameter);
  },

  patchRequest: async function (parameter: parameterType) {
    parameter.methodType = "PATCH";
    return this.request(parameter);
  },

  deleteRequest: async function (parameter: parameterType) {
    parameter.methodType = "DELETE";
    return this.request(parameter);
  },
};

export { mainAxios };
