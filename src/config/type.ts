export type parameterType = {
  methodType?: "GET" | "POST" | "PUT" | "PATCH" | "DELETE";
  url: string;
  payload?: any;
  requiresToken?: boolean;
  config?: any;
};
