import {
  FacebookOutlined,
  InstagramOutlined,
  TwitterOutlined,
  YoutubeOutlined,
} from "@ant-design/icons";
import { Button, Col, Image, Layout, Row, Space } from "antd";
import img from "asset/img/LogoHVKTMM.png";
import { HeaderChildren, NavBar } from "components";
import React from "react";
import "./style.scss";

interface ContainerProps {}

const Container: React.FC<ContainerProps> = ({ children }) => {
  const { Header, Footer, Content } = Layout;

  return (
    <Layout className="layout">
      <div className="container">
        <Header className=" header">
          <HeaderChildren />
        </Header>
      </div>
      <NavBar />
      <Content style={{ overflow: "hidden" }}>{children}</Content>
      <Footer className="footer">
        <Row justify="center">
          <Image preview={false} width={150} src={img} />
        </Row>
        <Row justify="center">
          <Col>
            <Space>
              <Button
                size="large"
                shape="circle"
                icon={<YoutubeOutlined />}
              ></Button>
              <Button
                size="large"
                shape="circle"
                icon={<TwitterOutlined />}
              ></Button>
              <Button
                size="large"
                shape="circle"
                icon={<InstagramOutlined />}
              ></Button>
              <Button
                size="large"
                shape="circle"
                icon={<FacebookOutlined />}
              ></Button>
            </Space>
          </Col>
        </Row>
      </Footer>
    </Layout>
  );
};

export default Container;
