import { Result } from "antd";
import Container from "container";
import React from "react";

const PageNotFound = () => {
  return (
    <Container>
      <div className="content">
        <Result
          status="404"
          title="404"
          subTitle="Xin lỗi, trang bạn muốn đến không tồn tại"
        />
      </div>
    </Container>
  );
};

export default PageNotFound;
