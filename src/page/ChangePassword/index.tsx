import {
  Breadcrumb,
  Button,
  Col,
  Divider,
  Form,
  Input,
  message,
  Row,
  Typography,
} from "antd";
import { changePassword, setupPasswordGoogle } from "api/auth";
import { MenuSideBar } from "components";
import Container from "container";
import React from "react";
import { listMenuProfile } from "utils/listMenu";
import { getLocalStorage } from "utils/localStorage";
import "./style.scss";

const { Title } = Typography;

const ChangePassword = () => {
  const code = getLocalStorage("code", 0);

  const handleChangePassword = async (values: any) => {
    const res: any = await changePassword(values);
    if (res && res.message) {
      message.success(res.message);
    }
  };

  const handleSetupPassword = async (values: any) => {
    const res: any = await setupPasswordGoogle(values);
    if (res && res.message) {
      message.success(res.message);
    }
  };

  return (
    <Container>
      <div className="content">
        <Row gutter={[20, 20]}>
          <Col span={4}>
            <MenuSideBar data={listMenuProfile} />
          </Col>
          <Col span={20}>
            <Breadcrumb>
              <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
              <Breadcrumb.Item>Đổi mật khẩu</Breadcrumb.Item>
            </Breadcrumb>
            <Title level={3}>Thay đổi mật khẩu</Title>
            <Divider />
            <Row justify="center" style={{ paddingTop: 30 }}>
              <Col span={10}>
                <Form
                  name="normal_login"
                  className="login-form"
                  layout="vertical"
                  onFinish={
                    code !== "SET_UP_PASSWORD"
                      ? handleChangePassword
                      : handleSetupPassword
                  }
                >
                  {code !== "SET_UP_PASSWORD" && (
                    <Form.Item
                      name="password"
                      label="Mật khẩu hiện tại"
                      rules={[
                        {
                          required: true,
                          message: "Mời bạn nhập mật khẩu!",
                        },
                      ]}
                    >
                      <Input.Password />
                    </Form.Item>
                  )}
                  <Form.Item
                    name="newPassword"
                    label="Mật khẩu mới"
                    rules={[
                      {
                        required: true,
                        message: "Please input your password!",
                      },
                      {
                        min: 8,
                        message: "Mật khẩu phải có ít nhất 8 ký tự",
                      },
                      {
                        pattern:
                          /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
                        message:
                          "Mật khẩu phải có chữ in hoa, chữ thường và chữ số",
                      },
                    ]}
                    hasFeedback
                  >
                    <Input.Password />
                  </Form.Item>

                  <Form.Item
                    name="confirm"
                    label="Xác nhận lại mật khẩu mới"
                    dependencies={["newPassword"]}
                    hasFeedback
                    rules={[
                      {
                        required: true,
                        message: "Please confirm your password!",
                      },
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (
                            !value ||
                            getFieldValue("newPassword") === value
                          ) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            new Error(
                              "The two passwords that you entered do not match!"
                            )
                          );
                        },
                      }),
                    ]}
                  >
                    <Input.Password />
                  </Form.Item>
                  <Form.Item>
                    <Button
                      type="primary"
                      htmlType="submit"
                      className="login-form-button"
                    >
                      Thay đổi
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </Container>
  );
};

export default ChangePassword;
