import {
  Breadcrumb,
  Button,
  Col,
  Divider,
  Form,
  Input,
  Row,
  Typography,
} from "antd";
import { MenuSideBar } from "components";
import Container from "container";
import React from "react";
import { listMenuAccount } from "utils/listMenu";
import "./style.scss";

const { Title } = Typography;

const SignIn = () => {
  return (
    <Container>
      <div className="content">
        <Row gutter={[20, 20]}>
          <Col span={4}>
            <MenuSideBar data={listMenuAccount} />
          </Col>
          <Col span={20}>
            <Breadcrumb>
              <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
              <Breadcrumb.Item>Đăng nhập</Breadcrumb.Item>
            </Breadcrumb>
            <Title level={3}>Quên mật khẩu</Title>
            <Divider />
            <Row justify="center" style={{ paddingTop: 30 }}>
              <Col span={10}>
                <Form
                  name="normal_login"
                  className="login-form"
                  // onFinish={onFinish}
                  layout="vertical"
                >
                  <Form.Item
                    name="email"
                    label="E-mail"
                    rules={[
                      {
                        type: "email",
                        message: "The input is not valid E-mail!",
                      },
                      {
                        required: true,
                        message: "Please input your E-mail!",
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item>
                    <Button
                      type="primary"
                      htmlType="submit"
                      className="login-form-button"
                    >
                      Gửi
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </Container>
  );
};

export default SignIn;
