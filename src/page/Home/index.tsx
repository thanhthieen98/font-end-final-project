import { Carousel, Col, Divider, Row, Select, Typography } from "antd";
import slide1 from "asset/img/slide1.jpg";
import slide2 from "asset/img/slide2.jpg";
import slide3 from "asset/img/slide3.jpg";
import { CardItem } from "components";
import Container from "container";
import React from "react";
import "./style.scss";

const { Title } = Typography;
const { Option } = Select;

const Home = () => {
  return (
    <Container>
      <Carousel autoplay arrows effect="fade">
        <div className="slide">
          <img src={slide1} alt="" />
        </div>
        <div className="slide">
          <img src={slide2} alt="" />
        </div>
        <div className="slide">
          <img src={slide3} alt="" />
        </div>
      </Carousel>
      <div className="content">
        <Title type="danger" level={3}>
          Sản phẩm
        </Title>
        <Divider />
        <Row justify="end" style={{ marginBottom: 15 }}>
          <Col>
            <span>Sắp xếp </span>
            <Select
              defaultValue="lucy"
              style={{ width: 120 }}
              // onChange={handleChange}
            >
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="disabled" disabled>
                Disabled
              </Option>
              <Option value="Yiminghe">yiminghe</Option>
            </Select>
          </Col>
        </Row>
        <Row gutter={[15, 15]}>
          <Col span={6}>
            <CardItem />
          </Col>
        </Row>
      </div>
    </Container>
  );
};

export default Home;
