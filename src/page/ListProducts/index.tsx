import { DoubleRightOutlined, UnorderedListOutlined } from "@ant-design/icons";
import { Breadcrumb, Col, Divider, Menu, Row, Select, Typography } from "antd";
import { CardItem } from "components";
import Container from "container";
import React from "react";
import "./style.scss";

const { Title } = Typography;
const { Option } = Select;

const ListProducts = () => {
  return (
    <Container>
      <div className="content">
        <Row gutter={[20, 20]}>
          <Col span={4}>
            <p className="category">
              <UnorderedListOutlined /> Danh mục sản phẩm
            </p>
            <Menu>
              <Menu.Item
                key="1"
                icon={<DoubleRightOutlined style={{ fontSize: 12 }} />}
              >
                Điện thoại
              </Menu.Item>
              <Menu.Item
                key="2"
                icon={<DoubleRightOutlined style={{ fontSize: 12 }} />}
              >
                Máy tính bảng
              </Menu.Item>
              <Menu.Item
                key="3"
                icon={<DoubleRightOutlined style={{ fontSize: 12 }} />}
              >
                Máy tính xách tay
              </Menu.Item>
            </Menu>
          </Col>
          <Col span={20}>
            <Breadcrumb>
              <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
              <Breadcrumb.Item>sản phẩm</Breadcrumb.Item>
            </Breadcrumb>
            <Title type="danger" level={3}>
              Sản phẩm
            </Title>
            <Divider />
            <Row justify="end" style={{ marginBottom: 15 }}>
              <Col>
                <span>Sắp xếp </span>
                <Select
                  defaultValue="lucy"
                  style={{ width: 120 }}
                  // onChange={handleChange}
                >
                  <Option value="jack">Jack</Option>
                  <Option value="lucy">Lucy</Option>
                  <Option value="disabled" disabled>
                    Disabled
                  </Option>
                  <Option value="Yiminghe">yiminghe</Option>
                </Select>
              </Col>
            </Row>
            <Row gutter={[15, 15]}>
              <Col span={6}>
                <CardItem />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </Container>
  );
};

export default ListProducts;
