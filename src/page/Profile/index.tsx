import {
  Breadcrumb,
  Button,
  Col,
  Descriptions,
  Divider,
  Drawer,
  Form,
  Input,
  Row,
  Select,
  Typography,
  message,
  Cascader,
  Spin,
} from "antd";
import { getProfile, updateProfile } from "api/user";
import { MenuSideBar } from "components";
import Container from "container";
import { residences } from "page/SignUp";
import React, { useEffect, useState } from "react";
import { listMenuProfile } from "utils/listMenu";
import "./style.scss";

const { Title } = Typography;
const { Option } = Select;

const convertData = (string: string) => {
  switch (string) {
    case "male":
      return "Nam";
    case "female":
      return "Nữ";

    default:
      return "Khác";
  }
};

const Profile = () => {
  const [visible, setVisible] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);
  const [data, setData] = useState<any>({});

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const res: any = await getProfile();
    if (res) {
      setData(res);
    } else {
      message.error("Đã có lỗi xảy ra!");
    }
    setLoading(false);
  };

  const updateData = async (values: any) => {
    setLoading(true);
    setVisible(false);
    delete values.email;
    const res: any = await updateProfile({
      ...values,
      address: values?.address?.toString(),
    });
    if (res) {
      setData(res);
    } else {
      message.error("Đã có lỗi xảy ra!");
    }
    setLoading(false);
  };

  return (
    <Container>
      <Spin spinning={loading}>
        <div className="content">
          <Row gutter={[20, 20]}>
            <Col span={4}>
              <MenuSideBar data={listMenuProfile} />
            </Col>
            <Col span={20}>
              <Breadcrumb>
                <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
                <Breadcrumb.Item>Thông tin chung</Breadcrumb.Item>
              </Breadcrumb>
              <Title level={3}>Thông tin tài khoản</Title>
              <Divider />
              <Descriptions column={1} bordered>
                <Descriptions.Item label="Tên">{data.name}</Descriptions.Item>
                <Descriptions.Item label="Email">
                  {data.email}
                </Descriptions.Item>
                <Descriptions.Item label="Giới tính">
                  {data.gender ? convertData(data.gender) : "..."}
                </Descriptions.Item>
                <Descriptions.Item label="Số điện thoại">
                  {data.phone || "..."}
                </Descriptions.Item>
                {/* <Descriptions.Item label="Ngày sinh">
                
              </Descriptions.Item> */}
                <Descriptions.Item label="Địa chỉ">
                  {data.address || "..."}
                </Descriptions.Item>
              </Descriptions>
              <Row justify="end">
                <Button
                  style={{ marginTop: 10 }}
                  type="primary"
                  className="btn-submit"
                  onClick={() => setVisible(true)}
                >
                  Cập nhật
                </Button>
              </Row>
            </Col>
          </Row>
        </div>
      </Spin>

      <Drawer
        title="Cập nhập thông tin"
        placement="right"
        onClose={() => setVisible(false)}
        visible={visible}
        width={400}
      >
        <Form
          name="normal_login"
          className="login-form"
          layout="vertical"
          onFinish={updateData}
          initialValues={{ ...data, address: data?.address?.split(",") }}
          hideRequiredMark
        >
          <Form.Item name="email" label="E-mail">
            <Input disabled />
          </Form.Item>

          <Form.Item
            name="name"
            label="Tên"
            tooltip="What do you want others to call you?"
            rules={[
              {
                required: true,
                message: "Please input your nickname!",
                whitespace: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="phone"
            label="Số điện thoại"
            rules={[
              {
                required: true,
                message: "Please input your phone number!",
              },
              {
                pattern: /(84|0[3|5|7|8|9])+([0-9]{8})\b/,
                message: "Đây không phải số điện thoại",
              },
            ]}
          >
            <Input addonBefore={"+84"} style={{ width: "100%" }} />
          </Form.Item>

          <Form.Item
            name="gender"
            label="Giới tính"
            rules={[{ required: true, message: "Please select gender!" }]}
          >
            <Select placeholder="Chọn giới tính">
              <Option value="male">Nam</Option>
              <Option value="female">Nữ</Option>
              <Option value="other">Khác</Option>
            </Select>
          </Form.Item>

          <Form.Item
            name="address"
            label="Địa chỉ"
            rules={[
              {
                type: "array",
                required: true,
                message: "Bạn chưa chọn địa chỉ",
              },
            ]}
          >
            <Cascader placeholder="Chọn địa chỉ" options={residences} />
          </Form.Item>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Lưu
            </Button>
          </Form.Item>
        </Form>
      </Drawer>
    </Container>
  );
};

export default Profile;
