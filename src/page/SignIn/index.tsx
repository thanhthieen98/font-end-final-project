import { LockOutlined, UserOutlined } from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Col,
  Divider,
  Form,
  Input,
  message,
  Row,
  Typography,
} from "antd";
import { loginGoogle, signIn } from "api/auth";
import { MenuSideBar } from "components";
import Container from "container";
import React, { useState } from "react";
import { useHistory } from "react-router";
import { listMenuAccount } from "utils/listMenu";
import { saveLocalStorage } from "utils/localStorage";
import "./style.scss";
import { GoogleLogin } from "react-google-login";
const { REACT_APP_GOOGLE_CLIENT_ID } = process.env;
const { Title } = Typography;
const SignIn = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const history = useHistory();

  const onFinish = async (values: any) => {
    setLoading(true);
    const data = await signIn(values);
    if (data && data.access_token) {
      saveLocalStorage("token", data.access_token, 0);
      saveLocalStorage("user", data.user, 0);
      history.push("/");
    } else {
      message.error("Email hoặc mật khẩu không đúng");
    }
    setLoading(false);
  };
  // const logInGoogle = () => {
  //   window.open("http://localhost:8000/auth/google", "_self");
  // };

  const onSuccess = async (res: any) => {
    const user = {
      email: res?.profileObj.email,
      name: res?.profileObj?.familyName + " " + res?.profileObj?.givenName,
      id: res?.profileObj?.googleId,
      token: res?.tokenId,
    };
    const data: any = await loginGoogle(user);
    if (data) {
      saveLocalStorage("token", data.access_token, 0);
      saveLocalStorage("user", data.user, 0);
      saveLocalStorage("code", data.code, 0);
      history.push("/");
    } else {
      message.error("Đã có lỗi xảy ra");
    }
  };
  const onFailure = () => {
    message.error("Đã có lỗi xảy ra");
  };

  return (
    <Container>
      <div className="content">
        <Row gutter={[20, 20]}>
          <Col span={4}>
            <MenuSideBar data={listMenuAccount} />
          </Col>
          <Col span={20}>
            <Breadcrumb>
              <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
              <Breadcrumb.Item>Đăng nhập</Breadcrumb.Item>
            </Breadcrumb>
            <Title level={3}>Đăng nhập vào hệ thống</Title>
            <Divider />
            <Row justify="center" style={{ paddingTop: 30 }}>
              <Col span={10}>
                <Form
                  name="normal_login"
                  className="login-form"
                  onFinish={onFinish}
                >
                  <Form.Item
                    name="email"
                    rules={[
                      // {
                      //   type: "email",
                      //   message: "The input is not valid E-mail!",
                      // },
                      {
                        required: true,
                        message: "Mời bạn nhập email!",
                      },
                    ]}
                  >
                    <Input
                      prefix={<UserOutlined className="site-form-item-icon" />}
                      placeholder="Email"
                    />
                  </Form.Item>
                  <Form.Item
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: "Mời bạn nhập mật khẩu!",
                      },
                    ]}
                  >
                    <Input
                      prefix={<LockOutlined className="site-form-item-icon" />}
                      type="password"
                      placeholder="Password"
                    />
                  </Form.Item>

                  <Form.Item>
                    <Button
                      type="primary"
                      htmlType="submit"
                      className="login-form-button"
                      loading={loading}
                    >
                      Log in
                    </Button>
                  </Form.Item>
                  <Divider> Hoặc </Divider>
                  {/* <div onClick={logInGoogle} className="google-btn">
                    <div className="google-icon-wrapper">
                      <img
                        alt="icon"
                        className="google-icon"
                        src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"
                      />
                    </div>
                    <p className="btn-text">
                      <b>Login in with google</b>
                    </p>
                  </div> */}

                  <GoogleLogin
                    clientId={REACT_APP_GOOGLE_CLIENT_ID!}
                    render={(renderProps) => (
                      <div onClick={renderProps.onClick} className="google-btn">
                        <div className="google-icon-wrapper">
                          <img
                            alt="icon"
                            className="google-icon"
                            src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"
                          />
                        </div>
                        <p className="btn-text">
                          <b>Login in with google</b>
                        </p>
                      </div>
                    )}
                    onSuccess={onSuccess}
                    onFailure={onFailure}
                    style={{ width: "100%" }}
                  />
                </Form>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </Container>
  );
};

export default SignIn;
