import {
  Breadcrumb,
  Button,
  Cascader,
  Col,
  Divider,
  Form,
  Input,
  message,
  Row,
  Select,
  Typography,
} from "antd";
import MenuSideBar from "components/Menu";
import Container from "container";
import React, { useState } from "react";
import { listMenuAccount } from "utils/listMenu";
import "./style.scss";
import ReCAPTCHA from "react-google-recaptcha";
import { signUp } from "api/auth";
import { useHistory } from "react-router";
import { default as local } from "utils/local.json";

const dataLocal: any = local;

const { Title } = Typography;
const { Option } = Select;

export const residences = dataLocal.map((item: any) => ({
  value: item.name,
  label: item.name,
  children: item.districts.map((el: any) => ({
    value: el.name,
    label: el.name,
  })),
}));

const SignIn = () => {
  const [token, setToken] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);

  const history = useHistory();
  const onFinish = async (values: any) => {
    setLoading(true);
    if (token) {
      const res = await signUp({
        ...values,
        address: values?.address?.toString(),
      });
      if (res) {
        history.push("/sign-in");
        message.success("Đăng ký thành công");
      } else {
        message.error("Đã có lỗi xảy ra");
      }
    } else {
      message.warning("Hãy xác thực mã capcha");
    }
    setLoading(false);
  };

  return (
    <Container>
      <div className="content">
        <Row gutter={[20, 20]}>
          <Col span={4}>
            <MenuSideBar data={listMenuAccount} />
          </Col>
          <Col span={20}>
            <Breadcrumb>
              <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
              <Breadcrumb.Item>Đăng ký</Breadcrumb.Item>
            </Breadcrumb>
            <Title level={3}>Đăng ký tài khoản</Title>
            <Divider />
            <Row justify="center">
              <Col span={10}>
                <Form
                  name="normal_login"
                  className="login-form"
                  layout="vertical"
                  onFinish={onFinish}
                  initialValues={{ gender: "male" }}
                >
                  <Form.Item
                    name="email"
                    label="E-mail"
                    rules={[
                      {
                        type: "email",
                        message: "The input is not valid E-mail!",
                      },
                      {
                        required: true,
                        message: "Please input your E-mail!",
                      },
                    ]}
                    hasFeedback
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    name="confirm"
                    label="Mật khẩu"
                    rules={[
                      {
                        required: true,
                        message: "Please input your password!",
                      },
                      {
                        min: 8,
                        message: "Mật khẩu phải có ít nhất 8 ký tự",
                      },
                      {
                        pattern:
                          /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
                        message:
                          "Mật khẩu phải có chữ in hoa, chữ thường và chữ số",
                      },
                    ]}
                    hasFeedback
                  >
                    <Input.Password />
                  </Form.Item>

                  <Form.Item
                    name="password"
                    label="Xác nhận mật khẩu"
                    dependencies={["confirm"]}
                    hasFeedback
                    rules={[
                      {
                        required: true,
                        message: "Please confirm your password!",
                      },
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (!value || getFieldValue("confirm") === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            new Error(
                              "The two passwords that you entered do not match!"
                            )
                          );
                        },
                      }),
                    ]}
                  >
                    <Input.Password />
                  </Form.Item>

                  <Form.Item
                    name="name"
                    label="Tên"
                    tooltip="What do you want others to call you?"
                    rules={[
                      {
                        required: true,
                        message: "Please input your nickname!",
                        whitespace: true,
                      },
                    ]}
                    hasFeedback
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    name="phone"
                    label="Số điện thoại"
                    rules={[
                      {
                        required: true,
                        message: "Please input your phone number!",
                      },
                      {
                        pattern: /(84|0[3|5|7|8|9])+([0-9]{8})\b/,
                        message: "Đây không phải số điện thoại",
                      },
                    ]}
                  >
                    <Input addonBefore={"+84"} style={{ width: "100%" }} />
                  </Form.Item>

                  <Form.Item name="gender" label="Giới tính">
                    <Select>
                      <Option value="male">Nam</Option>
                      <Option value="female">Nữ</Option>
                      <Option value="other">Khác</Option>
                    </Select>
                  </Form.Item>

                  <Form.Item
                    name="address"
                    label="Địa chỉ"
                    rules={[
                      {
                        type: "array",
                        required: true,
                        message: "Bạn chưa chọn địa chỉ",
                      },
                    ]}
                  >
                    <Cascader placeholder="Chọn địa chỉ" options={residences} />
                  </Form.Item>

                  <Form.Item
                    label="Captcha"
                    extra="We must make sure that your are a human."
                  >
                    <Row gutter={8}>
                      <Col span={24}>
                        <Form.Item
                          noStyle
                          rules={[
                            {
                              required: true,
                              message: "Please input the captcha you got!",
                            },
                          ]}
                        >
                          <ReCAPTCHA
                            // ref={recaptchaRef}
                            sitekey="6Lfco6odAAAAAH-LwAJ2GsMjAUU2GYNBjgxXVZBM"
                            onChange={(value: any) => setToken(value)}
                          />
                        </Form.Item>
                      </Col>
                      {/* <Col span={8}>
                        <Button style={{ width: "100%" }}>Get captcha</Button>
                      </Col> */}
                    </Row>
                  </Form.Item>

                  {/* <Form.Item
                    name="agreement"
                    valuePropName="checked"
                    rules={[
                      {
                        validator: (_, value) =>
                          value
                            ? Promise.resolve()
                            : Promise.reject(
                                new Error("Should accept agreement")
                              ),
                      },
                    ]}
                  >
                    <Checkbox>
                      I have read the <a href="">agreement</a>
                    </Checkbox>
                  </Form.Item> */}
                  <Form.Item>
                    <Button
                      type="primary"
                      htmlType="submit"
                      className="login-form-button"
                      loading={loading}
                    >
                      Đăng ký
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </Container>
  );
};

export default SignIn;
