export const listMenuAccount = {
  title: "Tài kkhoản",
  list: [
    {
      key: "/sign-in",
      name: "Đăng nhập",
    },
    {
      key: "/sign-up",
      name: "Đăng ký",
    },
    {
      key: "/forgot-password",
      name: "Quên mật khẩu",
    },
  ],
};

export const listMenuProfile = {
  title: "Thông tin cá nhân",
  list: [
    {
      key: "/profile",
      name: "Thông tin chung",
    },
    {
      key: "/change-password",
      name: "Đổi mật khẩu",
    },
    {
      key: "/order",
      name: "Đơn mua",
    },
  ],
};
